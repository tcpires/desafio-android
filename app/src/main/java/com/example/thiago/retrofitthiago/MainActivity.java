package com.example.thiago.retrofitthiago;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

import com.example.thiago.retrofitthiago.adapters.EndlessRecyclerViewScrollListener;
import com.example.thiago.retrofitthiago.adapters.GitHubAdapter;
import com.example.thiago.retrofitthiago.decorations.SimpleDividerItemDecoration;
import com.example.thiago.retrofitthiago.models.Constants;
import com.example.thiago.retrofitthiago.models.Repo;
import com.example.thiago.retrofitthiago.models.TopRepositories;
import com.example.thiago.retrofitthiago.services.GitHubService;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.ArrayList;
import java.util.List;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {

    final Gson gson = new GsonBuilder()
            .setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ")
            .create();

    final OkHttpClient.Builder okHttpClientBuilder = new OkHttpClient.Builder();

    final Retrofit.Builder retrofitBuilder = new Retrofit.Builder().baseUrl(Constants.API_BASE_URL)
            .addConverterFactory(GsonConverterFactory.create(gson));

    final Retrofit retrofit = retrofitBuilder.client(okHttpClientBuilder.build()).build();

    final GitHubService gitHubService = retrofit.create(GitHubService.class);

    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private RecyclerView.Adapter adapter;

    private List<Repo> repositories = new ArrayList<>();

    private EndlessRecyclerViewScrollListener scrollListener;

    private Integer page = 1;

    private Toolbar toolbar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        adapter = new GitHubAdapter(repositories);
        layoutManager = new LinearLayoutManager(this);

        recyclerView = (RecyclerView) findViewById(R.id.repos_recicle_view);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
        recyclerView.addItemDecoration(new SimpleDividerItemDecoration(this));

        toolbar = (Toolbar) findViewById(R.id.toolbar_pullrequest);
        setSupportActionBar(toolbar);
        toolbar.setTitleTextColor(Color.WHITE);

        showListRepos();

        scrollListener = new EndlessRecyclerViewScrollListener((LinearLayoutManager) layoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {

                loadNextDataFromApi();
            }
        };
        recyclerView.addOnScrollListener(scrollListener);
    }

    private void loadNextDataFromApi() {
        if (page >= 1) {
            page = page + 1;
            showListRepos();
        }
    }

    private void showListRepos() {
        Call<TopRepositories> topRepositoriesCall = gitHubService
                .getTopRepositories("language:Java", "stars", page);

        topRepositoriesCall.enqueue(new Callback<TopRepositories>() {
            @Override
            public void onResponse(Call<TopRepositories> call, final Response<TopRepositories> response) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        repositories.addAll(response.body().getRepositories());
                        adapter.notifyDataSetChanged();
                    }
                });
            }

            @Override
            public void onFailure(Call<TopRepositories> call, Throwable t) {
                Log.i("Erro Repos", "Erro: " + t.getMessage());
                Toast.makeText(getApplicationContext(), "Falha na Chamada dos repositórios: "
                                + t.getMessage(),
                        Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.repo_exit:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}