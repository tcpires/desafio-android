package com.example.thiago.retrofitthiago.services;

import com.example.thiago.retrofitthiago.models.PullRequest;
import com.example.thiago.retrofitthiago.models.TopRepositories;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface GitHubService {

    @GET("/search/repositories")
    Call<TopRepositories> getTopRepositories(@Query("q") String q, @Query("sort") String sort, @Query("page") Integer page);

    @GET("repos/{creator}/{repository}/pulls")
    Call<List<PullRequest>> getPulls(@Path("creator") String creator, @Path("repository") String repositorie);
}
