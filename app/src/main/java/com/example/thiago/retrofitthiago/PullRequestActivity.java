package com.example.thiago.retrofitthiago;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.example.thiago.retrofitthiago.adapters.PullRequestAdapter;
import com.example.thiago.retrofitthiago.decorations.SimpleDividerItemDecoration;
import com.example.thiago.retrofitthiago.models.Constants;
import com.example.thiago.retrofitthiago.models.PullRequest;
import com.example.thiago.retrofitthiago.services.GitHubService;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.List;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class PullRequestActivity extends AppCompatActivity {

    final Gson gson = new GsonBuilder()
            .setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ")
            .create();

    final OkHttpClient.Builder okHttpClientBuilder = new OkHttpClient.Builder();

    final Retrofit.Builder retrofitBuilder = new Retrofit.Builder().baseUrl(Constants.API_BASE_URL)
            .addConverterFactory(GsonConverterFactory.create(gson));

    final Retrofit retrofit = retrofitBuilder.client(okHttpClientBuilder.build()).build();

    final GitHubService gitHubService = retrofit.create(GitHubService.class);

    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private PullRequestAdapter adapter;

    private String creator;
    private String repositories;

    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pull_request);

        layoutManager = new LinearLayoutManager(this);

        recyclerView = (RecyclerView) findViewById(R.id.pulls_recycle_view);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.addItemDecoration(new SimpleDividerItemDecoration(this));

        Bundle bundle = getIntent().getExtras();
        creator = bundle.getString("OWNER");
        repositories = bundle.getString("REPO");

        toolbar = (Toolbar) findViewById(R.id.toolbar_pullrequest);
        toolbar.setNavigationIcon(R.drawable.ic_action_goleft);
        toolbar.setTitle(repositories.toString());
        toolbar.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toolbar);

        showListPull();
    }

    private void showListPull() {
        Call<List<PullRequest>> pullRequestsCall = gitHubService
                .getPulls(creator, repositories);

        pullRequestsCall.enqueue(new Callback<List<PullRequest>>() {
            @Override
            public void onResponse(Call<List<PullRequest>> call, final Response<List<PullRequest>> response) {

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        List<PullRequest> pullRequestses = (List<PullRequest>) response.body();
                        adapter = new PullRequestAdapter(pullRequestses);
                        adapter.notifyDataSetChanged();
                        recyclerView.setAdapter(adapter);
                    }
                });
            }

            @Override
            public void onFailure(Call<List<PullRequest>> call, Throwable t) {
                Log.i("Erro Pulls", "Erro: " + t.getMessage());
                Toast.makeText(getApplicationContext(), "Falha na Chamada dos pullRequests: "
                                + t.getMessage(),
                        Toast.LENGTH_LONG).show();
            }
        });
    }

}
