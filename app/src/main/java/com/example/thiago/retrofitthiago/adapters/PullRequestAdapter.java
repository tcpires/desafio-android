package com.example.thiago.retrofitthiago.adapters;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.thiago.retrofitthiago.R;
import com.example.thiago.retrofitthiago.models.PullRequest;
import com.squareup.picasso.Picasso;

import android.text.format.DateFormat;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class PullRequestAdapter extends RecyclerView.Adapter<PullRequestAdapter.ViewHolder> {

    private List<PullRequest> pullRequests;

    public PullRequestAdapter(List<PullRequest> pullRequests) {

        this.pullRequests = pullRequests;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.pull_view_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        viewHolder.pullTitle = (TextView) view.findViewById(R.id.pull_title);
        viewHolder.pullDescription = (TextView) view.findViewById(R.id.pull_description);
        viewHolder.pullUserAvatar = (CircleImageView) view.findViewById(R.id.circle_image);
        viewHolder.pullUser = (TextView) view.findViewById(R.id.pull_user);
        viewHolder.pullDate = (TextView) view.findViewById(R.id.pull_date);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, final int position) {
        viewHolder.pullTitle.setText(pullRequests.get(position).getTitle());
        viewHolder.pullDescription.setText(pullRequests.get(position).getBody());
        Picasso.with(viewHolder.pullUserAvatar.getContext()).load(pullRequests.get(position)
                .getUser().getAvatarUrl()).fit().centerCrop().into(viewHolder.pullUserAvatar);
        viewHolder.pullUser.setText(pullRequests.get(position).getUser().getLogin());
        CharSequence pullDate = DateFormat.format("dd-MM-yyyy kk:mm:ss", pullRequests.get(position).getCreated_at());
        viewHolder.pullDate.setText(pullDate);

        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String url = pullRequests.get(position).getHtmlUrl();
                Uri webpage = Uri.parse(url);
                Intent intent = new Intent(Intent.ACTION_VIEW, webpage);
                v.getContext().startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return pullRequests != null ? pullRequests.size() : 0;
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView pullTitle;
        public TextView pullDescription;
        public CircleImageView pullUserAvatar;
        public TextView pullUser;
        public TextView pullDate;


        public ViewHolder(final View view) {
            super(view);
            pullTitle = (TextView) view.findViewById(R.id.pull_title);
            pullDescription = (TextView) view.findViewById(R.id.pull_description);
            pullUserAvatar = (CircleImageView) view.findViewById(R.id.circle_image);
            pullUser = (TextView) view.findViewById(R.id.pull_user);
            pullDate = (TextView) view.findViewById(R.id.pull_date);
        }
    }
}


