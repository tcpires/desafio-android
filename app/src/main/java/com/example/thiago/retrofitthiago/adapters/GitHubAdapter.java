package com.example.thiago.retrofitthiago.adapters;


import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.thiago.retrofitthiago.PullRequestActivity;
import com.example.thiago.retrofitthiago.R;
import com.example.thiago.retrofitthiago.models.Repo;
import com.squareup.picasso.Picasso;

import java.util.List;

public class GitHubAdapter extends RecyclerView.Adapter<GitHubAdapter.ViewHolder> {

    private List<Repo> repositories;

    public GitHubAdapter(List<Repo> repositorios) { this.repositories = repositorios; }

    @Override
    public GitHubAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.repo_view_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        viewHolder.repositoryName = (TextView) view.findViewById(R.id.repo_name);
        viewHolder.descriptionText = (TextView) view.findViewById(R.id.repo_description);
        viewHolder.forksNumber = (TextView) view.findViewById(R.id.repo_forks);
        viewHolder.starsNumber = (TextView) view.findViewById(R.id.repo_stars);
        viewHolder.ownerPhoto = (ImageView) view.findViewById(R.id.owner_photo);
        viewHolder.ownerName = (TextView) view.findViewById(R.id.owner_name);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, final int position) {
        viewHolder.repositoryName.setText(repositories.get(position).getName());
        viewHolder.descriptionText.setText(repositories.get(position).getDescription());
        viewHolder.forksNumber.setText(String.valueOf(repositories.get(position).getForks()));
        viewHolder.starsNumber.setText(String.valueOf(repositories.get(position).getStargazersCount()));
        Picasso.with(viewHolder.ownerPhoto.getContext()).load(repositories.get(position)
                .getOwner().getAvatarUrl()).into(viewHolder.ownerPhoto);
        viewHolder.ownerName.setText(repositories.get(position).getOwner().getLogin());

        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(v.getContext(), PullRequestActivity.class);
                String repo_name = repositories.get(position).getName();
                String owner_name = repositories.get(position).getOwner().getLogin();
                intent.putExtra("REPO", repo_name);
                intent.putExtra("OWNER", owner_name);
                v.getContext().startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return repositories != null ? repositories.size() : 0;
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView repositoryName;
        public TextView descriptionText;
        public TextView forksNumber;
        public TextView starsNumber;
        public ImageView ownerPhoto;
        public TextView ownerName;


        public ViewHolder(View view) {
            super(view);
            repositoryName = (TextView) view.findViewById(R.id.repo_name);
            descriptionText = (TextView) view.findViewById(R.id.repo_description);
            forksNumber = (TextView) view.findViewById(R.id.repo_forks);
            starsNumber = (TextView) view.findViewById(R.id.repo_stars);
            ownerPhoto = (ImageView) view.findViewById(R.id.owner_photo);
            ownerName = (TextView) view.findViewById(R.id.owner_name);

        }
    }
}