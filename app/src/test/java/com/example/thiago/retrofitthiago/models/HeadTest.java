package com.example.thiago.retrofitthiago.models;


import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class HeadTest {

    private Head head;
    private Repo repo;

    @Before
    public void setUp() throws Exception {
        repo = new Repo();

        head = new Head();
        head.setLabel("label");
        head.setRef("ref");
        head.setRepo(repo);
    }

    @Test
    public void head_ShouldBeNotNull() throws Exception {
        Assert.assertNotNull(head);
    }

    @Test
    public void getLabel_ShouldBeNotNull() throws Exception {
        Assert.assertNotNull(head.getLabel());
    }

    @Test
    public void getRef_ShouldBeNotNull() throws Exception {
        Assert.assertNotNull(head.getRef());
    }

    @Test
    public void getRepoShouldBeNotNull() throws Exception {
        Assert.assertNotNull(head.getRepo());
    }
}
