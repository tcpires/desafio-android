package com.example.thiago.retrofitthiago.services;

import com.example.thiago.retrofitthiago.models.Constants;
import com.example.thiago.retrofitthiago.models.PullRequest;
import com.example.thiago.retrofitthiago.models.TopRepositories;
import com.google.gson.GsonBuilder;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class GitHubServiceTest {

    private OkHttpClient.Builder okHttpClientBuilder;

    private Retrofit.Builder retrofitBuilder;

    private Retrofit retrofit;

    private GitHubService gitHubInterface;

    @Before
    public void setUp() {
        this.okHttpClientBuilder = new OkHttpClient.Builder();

        this.retrofitBuilder = new Retrofit.Builder()
                .baseUrl(Constants.API_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(new GsonBuilder()
                        .setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ")
                        .create()));

        this.retrofit = this.retrofitBuilder.client(okHttpClientBuilder.build()).build();

        this.gitHubInterface = retrofit.create(GitHubService.class);
    }

    @Test
    public void getTopRepositories_ShouldBeNotNull() {
        Call<TopRepositories> topRepositoriesCall = gitHubInterface
                .getTopRepositories("language:Java", "stars", 1);

        topRepositoriesCall.enqueue(new Callback<TopRepositories>() {
            @Override
            public void onResponse(Call<TopRepositories> call, Response<TopRepositories> response) {
                Assert.assertNotNull(response.body());
                Assert.assertNotNull(response.body());
            }

            @Override
            public void onFailure(Call<TopRepositories> call, Throwable t) {

            }
        });
    }

    @Test
    public void getTopRepositories_ShouldBeNotEmpty() {
        Call<TopRepositories> topRepositoriesCall = gitHubInterface
                .getTopRepositories("language:Java", "stars", 1);

        topRepositoriesCall.enqueue(new Callback<TopRepositories>() {
            @Override
            public void onResponse(Call<TopRepositories> call, Response<TopRepositories> response) {
                Assert.assertNotEquals(response.body().getRepositories().size(), 0);
            }

            @Override
            public void onFailure(Call<TopRepositories> call, Throwable t) {

            }
        });
    }

    @Test
    public void getPulls_ShouldBeNotNull() {
        Call<List<PullRequest>> pullRequestsCall = gitHubInterface
                .getPulls("tcpires", "RetrofitThiago");

        pullRequestsCall.enqueue(new Callback<List<PullRequest>>() {
            @Override
            public void onResponse(Call<List<PullRequest>> call, Response<List<PullRequest>> response) {
                Assert.assertNotNull(response.body());
            }

            @Override
            public void onFailure(Call<List<PullRequest>> call, Throwable t) {

            }
        });
    }

    @Test
    public void getPulls_ShouldBeNotEmpty() {
        Call<List<PullRequest>> pullRequestsCall = gitHubInterface
                .getPulls("tcpires", "RetrofitThiago");

        pullRequestsCall.enqueue(new Callback<List<PullRequest>>() {
            @Override
            public void onResponse(Call<List<PullRequest>> call, Response<List<PullRequest>> response) {
                Assert.assertNotEquals(response.body().size(), 0);
            }

            @Override
            public void onFailure(Call<List<PullRequest>> call, Throwable t) {

            }
        });
    }

}
